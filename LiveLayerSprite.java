package samodelkin.androidsimplestgamelibrary;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

/**
 * ����� ��� �������� �������� � �������� �����.
 * @autor samodelkin
 * @version 1.0
 */
public class LiveLayerSprite extends LayerSprite{
	private int maxLive; //������������ �����
	private int live; //������� �����
	private int horizontalZoom; //�������� ���������� �� ������� ��� ������ ������� ����� ����� ������ �������� �����.
	private int contourColor; //���� ������� ������� �����
	private int fillColor; //���� ����� ������� �����
	private int lineHeight; //������ ������� �����
	
	/**������� ������������� ������ � �������� �����.*/
	public LiveLayerSprite(int id, Resources res,int maxLive) {
		super(id, res);
		this.maxLive=maxLive;
		live=maxLive;
		horizontalZoom=1;
		contourColor=Color.BLACK;
		fillColor=Color.RED;
		lineHeight=5;
	}
	
	/**������� ������������ ������ � �������� �����.*/
	public LiveLayerSprite(int[] id, Resources res,int maxLive) {
		super(id, res);
		this.maxLive=maxLive;
		live=maxLive;
		horizontalZoom=1;
		contourColor=Color.BLACK;
		fillColor=Color.RED;
		lineHeight=5;
	}
	
	/**��������� �����.*/
	public void addLive(int live){
		this.live+=live;
	}
	
	/**�������� �����.*/
	public int getLive(){
		return live;
	}
	
	/**�������� ������������ �������� �����.*/
	public int getMaxLive(){
		return maxLive;
	}
	
	/**��������� �����.*/
	public void incLive(int live){
		this.live-=live;
	}
	
	/**����������.*/
	public void paint(Canvas c){
		super.paint(c);
		Paint p=new Paint();
		p.setStyle(Style.FILL);
		p.setColor(fillColor);
		c.drawRect(getX(),getY()-5-lineHeight,getX()+(live/horizontalZoom),getY()-5, p);
		p.setStyle(Style.STROKE);
		p.setColor(contourColor);
		c.drawRect(getX(),getY()-5-lineHeight,getX()+(maxLive/horizontalZoom),getY()-5, p);
	}
	
	/**�������� ����� �� ������������� ��������.*/
	public void resetLive(){
		live=maxLive;
	}
	
	/**���������� ���� ������� ������� �����.*/
	public void setContourColor(int color){
		contourColor=color;
	}
	
	/**���������� ���� ������� �����.*/
	public void setFillColor(int color){
		fillColor=color;
	}
	
	/**���������� ���������� ���, �� ������� ������ ������� ����� ����� ������ �������� �����.*/
	public void setHorizontalZoom(int horizontalZoom){
		this.horizontalZoom=horizontalZoom;
	}
	
	/**���������� �����.*/
	public void setLive(int live){
		this.live=live;
	}
	
	/**���������� ������������ �����.*/
	public void setMaxLive(int live){
		this.maxLive=live;
	}
	
	/**���������� ������ ������� �����.*/
	public void setLineHeight(int lineHeight){
		this.lineHeight=lineHeight;
	}
	

}

