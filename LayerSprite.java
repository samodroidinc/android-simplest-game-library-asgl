package samodelkin.androidsimplestgamelibrary;

import android.content.res.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;

/**
 * @autor samodelkin
 * @version 1.0
 */

public class LayerSprite extends Layer {
	private Bitmap[] mFrames;
	private int mFrameIndex;
	private int[] mFrameSequence;
	private Rect mBounds;
	private Matrix matrix;
	private int degress, refPixelX, refPixelY;

	public LayerSprite(int id, Resources res) {
		super();
		Bitmap bitmap = BitmapFactory.decodeResource(res, id);
		Bitmap[] bitmaps = new Bitmap[]{bitmap};
		mFrames = new Bitmap[bitmaps.length];
		System.arraycopy(bitmaps, 0, mFrames, 0, bitmaps.length);
		mFrameIndex = 0;
		mFrameSequence = new int[bitmaps.length];
		for (int n = 0; n < bitmaps.length; n++) mFrameSequence[n] = n; 
		mBounds = new Rect(0, 0, bitmaps[0].getWidth(), bitmaps[0].getHeight());
		matrix = new Matrix();
		degress = 0;
		refPixelX = this.getWidth() / 2;
		refPixelY = this.getHeight() / 2;
	}

	public LayerSprite(int[] id, Resources res) {
		super();
		Bitmap[] bitmaps;
		bitmaps = new Bitmap[id.length];
		for (int i = 0; i < id.length; i++) {
			bitmaps[i] = BitmapFactory.decodeResource(res, id[i]);
		}
		mFrames = new Bitmap[bitmaps.length];
		System.arraycopy(bitmaps, 0, mFrames, 0, bitmaps.length);
		mFrameIndex = 0;
		mFrameSequence = new int[bitmaps.length];
		for (int n = 0; n < bitmaps.length; n++) mFrameSequence[n] = n; 
		mBounds = new Rect(0, 0, bitmaps[0].getWidth(), bitmaps[0].getHeight());
		matrix = new Matrix();
		degress = 0;
		refPixelX = this.getWidth() / 2;
		refPixelY = this.getHeight() / 2;
	}

	public boolean collidesWith(LayerSprite s) {
		if (this.isVisible())
			return Rect.intersects(getBoundsRect(), s.getBoundsRect());
		else
			return false;
	}
	
	public boolean collidesWith(LayerTiles layerTiles)
	{
	    boolean b = false;
	    for (int col = 1; col < layerTiles.getCols() + 1; col++) {
			for (int row = 1; row < layerTiles.rows + 1; row++) {
				if (layerTiles.cells[(row * layerTiles.cols - (layerTiles.cols - col) - 1)] != 0) {
					if (Rect.intersects(layerTiles.getRect(col - 1, row - 1), this.getBoundsRect())) {
						b = true;
					}
				}
			}
	    }
	    return b;
	}
	
	
	public boolean collidesPixels(LayerSprite ls){
	if (collidesWith(ls)){
	//получаем картинки с каждого спрайта с учетом трансформации матрицы
	Bitmap b1=Bitmap.createBitmap(mFrames[mFrameIndex],0,0,getWidth(),getHeight(),this.matrix,false);
	Bitmap b2=Bitmap.createBitmap(ls.mFrames[ls.mFrameIndex],0,0,ls.getWidth(),ls.getHeight(),ls.matrix,false);
	return isCollisionDetected(b1,getX(),getY(),b2,ls.getX(),ls.getY());
	}
	return false;
	}
		
	private boolean isCollisionDetected(Bitmap bitmap1, int x1, int y1,
											  Bitmap bitmap2, int x2, int y2) {

		Rect bounds1 = new Rect(x1, y1, x1+bitmap1.getWidth(), y1+bitmap1.getHeight());
		Rect bounds2 = new Rect(x2, y2, x2+bitmap2.getWidth(), y2+bitmap2.getHeight());

		if (Rect.intersects(bounds1, bounds2)) {
			Rect collisionBounds = getCollisionBounds(bounds1, bounds2);
			for (int i = collisionBounds.left; i < collisionBounds.right; i++) {
				for (int j = collisionBounds.top; j < collisionBounds.bottom; j++) {
					int bitmap1Pixel = bitmap1.getPixel(i-x1, j-y1);
					int bitmap2Pixel = bitmap2.getPixel(i-x2, j-y2);
					if (isFilled(bitmap1Pixel) && isFilled(bitmap2Pixel)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	
	private Rect getCollisionBounds(Rect rect1, Rect rect2) {
		int left = (int) Math.max(rect1.left, rect2.left);
		int top = (int) Math.max(rect1.top, rect2.top);
		int right = (int) Math.min(rect1.right, rect2.right);
		int bottom = (int) Math.min(rect1.bottom, rect2.bottom);
		return new Rect(left, top, right, bottom);
	}

	private boolean isFilled(int pixel) {
		return pixel != Color.TRANSPARENT;
	}
	
	
	
		
		
		
	
	
	public boolean collidesWith(MotionEvent me) {
		boolean b = false;
		Point p = new Point();
		p.set((int) me.getX(), (int) me.getY());
		if (this.collidesWith(p))
			b = true;
		if (this.isVisible())
			return b;
		else
			return false;
	}

	public boolean collidesWith(Point point) {
		if (this.isVisible())
			return Rect.intersects(getBoundsRect(), new Rect(point.x, point.y,
					point.x, point.y));
		else
			return false;
	}

	public boolean collidesWith(Rect rect) {
		if (this.isVisible())
			return Rect.intersects(getBoundsRect(), rect);
		else
			return false;
	}

	public void defineCollisionRect(Rect rect) {
		mBounds.set(rect);
	}

	public Rect getBoundsRect() {
		Rect bounds = new Rect(mBounds);
		bounds.offset(this.getX(), this.getY());
		return bounds;
	}

	public int getFrameIndex() {
		return mFrameIndex;
	}

	public int getFrameSequenceLength() {
		return mFrameSequence.length;
	}

	public int getHeight() {
		return mFrames[0].getHeight();
	}

	public int getRefX() {
		return this.refPixelX;
	}

	public int getRefY() {
		return this.refPixelY;
	}

	public int getTurn() {
		return this.degress;
	}

	public int getWidth() {
		return mFrames[0].getWidth();
	}

	public void move(int dx, int dy) {
		super.move(dx, dy);
		matrix.reset();
		matrix.setTranslate(getX(),getY());
		matrix.postRotate(degress,getX()+refPixelX,getY()+refPixelY);
	}

	public void nextFrame() {
		mFrameIndex++;
		if (mFrameIndex == mFrameSequence.length) {
			mFrameIndex = 0;
		}
	}

	public void paint(Canvas canvas) {
		if (this.isVisible())
			canvas.drawBitmap(mFrames[mFrameSequence[mFrameIndex]], matrix,
					null);
	}

	public void prevFrame() {
		mFrameIndex--;
		if (mFrameIndex < 0) {
			mFrameIndex = mFrameSequence.length - 1;
		}
	}

	public void resetRotate() {
		degress = 0;
		matrix.reset();
		this.setPosition(getX(),getY());
	}

	public void rotate(int degress) {
		this.degress += degress;
		if (this.degress<0) this.degress=360+this.degress;
		setPosition(getX(),getY());
	}

	public void setFrame(int frame) {
		this.mFrameIndex = frame;
	}

	public void setFrameSequence(int[] frameSequence) {
		mFrameSequence = new int[frameSequence.length];
		System.arraycopy(frameSequence, 0, mFrameSequence, 0,
				frameSequence.length);
	}

	public void setPosition(int x, int y) {
		super.setPosition(x, y);
		matrix.reset();
		matrix.setTranslate(getX(),getY());
		matrix.postRotate(degress,getX()+refPixelX,getY()+refPixelY);
	}

	public void setRefPixel(int refX, int refY) {
		this.refPixelX = refX;
		this.refPixelY = refY;
	}

}
