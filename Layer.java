package samodelkin.androidsimplestgamelibrary;

/**
 * ����������� ����� ��� �������� ������� �������.
 * 
 * @autor samodelkin
 * @version 1.0
 */
import android.graphics.*;

public abstract class Layer
 {
	private int mX, mY; // ���������� ����
	private boolean mVisible; // ��������� ����

	/** ������� ����. */
	public Layer() {
		mX = mY = 0;
		mVisible = true;
	}

	/** �������� ���������� ���. */
	public int getX() {
		return mX;
	}

	/** �������� ���������� �����. */
	public int getY() {
		return mY;
	}

	/** �������� ��������� ����. */
	public boolean isVisible() {
		return mVisible;
	}

	/** ����������� �� ��������� ���������� ��������. */
	public void move(int dx, int dy) {
		mX += dx;
		mY += dy;
	}

	/** ���������� �������. */
	public void setPosition(int x, int y) {
		this.mX = x;
		this.mY = y;
	}

	/** ���������� ��������� ����. */
	public void setVisible(boolean visible) {
		mVisible = visible;
	}
	
	abstract void paint(Canvas c);

}
