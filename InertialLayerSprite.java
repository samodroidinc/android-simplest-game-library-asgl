package samodelkin.androidsimplestgamelibrary;

import android.content.res.Resources;

/**
 * ����� ��� �������� �������� � ��������.
 * @autor samodelkin
 * @version 1.0
 */
public class InertialLayerSprite extends LayerSprite{
	private double speedX,speedY; //�������� �� ����
	
	/**������� ������ � �������� � ����� ������.*/
	public InertialLayerSprite(int id, Resources res){
		super(id,res);
		speedX=speedY=0;
	}
	
	/**������� ������ � �������� � �������.*/
	public InertialLayerSprite(int[] id, Resources res){
		super(id,res);
		speedX=speedY=0;
	}
	
	/**�������� ������� �� ���������.*/
	public void addInertionY(double inertionY){
		this.speedY+=inertionY;
	}
	
	/**�������� ������� �� �����������.*/
	public void addInertionX(double inertionX){
		this.speedX+=inertionX;
	}
	
	/**����� ������� �� ������������ ���.*/
	public void resetInertionY(){
		this.speedY=0;
	}
	
	/**����� ������� �� �������������� ���.*/
	public void resetInertionX(){
		this.speedX=0;
	}
	
	/**����� ������� �� ���� ����.*/
	public void resetInertionAll(){
		this.speedY=this.speedX=0;
	}
	
	/**���������� ������� �� �������������� ���.*/
	public void setInertionX(double inertionX){
		this.speedX=inertionX;
	}
	
	/**���������� ������� �� ������������ ���.*/
	public void setInertionY(double inertionY){
		this.speedY=inertionY;
	}
	
	/**�������� ������� �� �������������� ���.*/
	public double getInertionX(){
		return speedX;
	}
	
	/**�������� ������� �� ������������ ���.*/
	public double getInertionY(){
		return speedY;
	}
	
	/**���������� ��������� ������� � ������ �������.*/
	public void update(){
		move((int)speedX,(int)speedY);
	}
	
}
