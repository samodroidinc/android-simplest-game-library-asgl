package samodelkin.androidsimplestgamelibrary;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

public class LayerTiles extends Layer {
	private Bitmap[] tiles;
	protected int[] cells;
	protected int cols;
	protected int rows;

	public LayerTiles(Resources res, int[] id, int[] cells, int cols, int rows) {
		this.tiles = new Bitmap[id.length];
		for (int i = 0; i < id.length; i++) {
			this.tiles[i] = BitmapFactory.decodeResource(res, id[i]);
		}
		this.cells = new int[cells.length];
		this.cells = cells;
		this.cols = cols;
		this.rows = rows;
	}

	public boolean collidesWith(LayerSprite layerSprite)
	  {
	    boolean b = false;
	    for (int col = 1; col < this.cols + 1; col++) {
	      for (int row = 1; row < this.rows + 1; row++) {
	        if (this.cells[(row * this.cols - (this.cols - col) - 1)] != 0) {
	          if (Rect.intersects(getRect(col - 1, row - 1), layerSprite.getBoundsRect())) {
	            b = true;
	          }
	        }
	      }
	    }
	    return b;
	  }

	public int getCols() {
		return this.cols;
	}

	public int getRows() {
		return this.rows;
	}

	public int getTileHeight() {
		return this.tiles[0].getHeight();
	}

	public int getTileWidth() {
		return this.tiles[0].getWidth();
	}

	public void paint(Canvas c) {
		for (int col = 1; col < this.cols + 1; col++) {
			for (int row = 1; row < this.rows + 1; row++) {
				if (this.cells[(row * this.cols - (this.cols - col) - 1)] != 0) {
					c.drawBitmap(this.tiles[(this.cells[(row * this.cols
							- (this.cols - col) - 1)] - 1)], this.getX()+(col - 1)
							* getTileWidth(),this.getY()+ (row - 1) * getTileHeight(), null);
				}
			}
		}
	}

	public void setCell(int col, int row, int index) {
		this.cells[getNumberCell(col, row)] = index;
	}

	public void setCells(int[] cells) {
		this.cells = new int[cells.length];
		this.cells = cells;
	}

	private int getNumberCell(int col, int row) {
		return (row * this.cols) - (this.cols - col) - 1;
	}

	protected Rect getRect(int col, int row) {
		Rect rect = new Rect(this.getX()+col * getTileWidth(),this.getY()+ row * getTileHeight(), this.getX()+col
				* getTileWidth() + getTileWidth(), this.getY()+row * getTileHeight()
				+ getTileHeight());
		return rect;
	}
}
