package samodelkin.androidsimplestgamelibrary;
import android.graphics.*;
import java.util.*;

public class LayerChief extends Layer
{
private Vector<Layer> layers;
private int xView,yView;

public LayerChief(){
super();
layers=new Vector<Layer>();
xView=yView=0;
}

public void append(Layer layer){
layers.add(layer);
}

public void insert(Layer layer,int index){
layers.insertElementAt(layer,index);
}

public void remove(Layer layer){
layers.remove(layer);
}

public void setView(int xView,int yView){
this.xView=xView;
this.yView=yView;
}

public void paint(Canvas c){
c.translate(-xView,-yView);
for(int i=0;i<layers.size();i++){
layers.elementAt(i).paint(c);
}
c.translate(0,0);
}

public int getSize(){
return layers.size();
}

public Layer getLayerAt(int index){
return layers.elementAt(index);
}

public void move(int dx,int dy){
for(int i=0;i<layers.size();i++) layers.elementAt(i).move(dx,dy);
}

}
