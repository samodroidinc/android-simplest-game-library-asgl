package samodelkin.androidsimplestgamelibrary;

import android.app.*;
import android.os.*;
import android.view.*;
import android.content.pm.*;

public class GameActivity extends Activity
{

public void onCreate(Bundle savedInstance){
super.onCreate(savedInstance);
requestWindowFeature(Window.FEATURE_NO_TITLE);
getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);		
}

public Display getDisplay(){
return this.getWindowManager().getDefaultDisplay();
}

public void setScreenMode(int mode){
switch(mode){
case 0:setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		
break;		
case 1:setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);		
break;	
}
}

}
