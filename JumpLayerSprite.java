package samodelkin.androidsimplestgamelibrary;

import android.content.res.Resources;

/**
 * ����� ��� �������� ��������� ��������.
 * @autor samodelkin
 * @version 1.0
 */
public class JumpLayerSprite extends InertialLayerSprite{
	private double gravitation; //��������� ���������� ������� � ��������,������������ ���� ������
	private boolean jumping; //��������� ������
	private double jumpSpeed; //��������� �������� ������ � ��������, ������������ �����

	/**������� ��������� ������ � ����� ������.*/
	public JumpLayerSprite(int id, Resources res) {
		super(id, res);
		gravitation=1;
		jumping=false;
		jumpSpeed=-10;
	}
	
	/**������� ��������� ������ � ����������� �������.*/
	public JumpLayerSprite(int[] id, Resources res) {
		super(id, res);
		gravitation=1;
		jumping=false;
		jumpSpeed=-10;
	}
	
	/**��������.*/
	public void jump(){
		jumping=true;
		this.setInertionY(jumpSpeed);
	}
	
	/**���������� ������� � ������ ������.*/
	public void update(){
		if (jumping){
			super.update();
			this.addInertionY(gravitation);
		}
	}
	
	/**�������� ��������� ������.*/
	public boolean isJumping(){
		return jumping;
	}
	
	/**���������� ����������.*/
	public void setGravitation(double gravitation){
		this.gravitation=gravitation;
	}
	
	/**�������� ����������.*/
	public double getGravitation(){
		return gravitation;
	}
	
	/**���������� ��������� �������� ������, ������������ �����.*/
	public void setJumpSpeed(double jumpSpeed){
		this.jumpSpeed=jumpSpeed;
	}
	
	/**�������� ��������� �������� ������.*/
	public double getJumpSpeed(){
		return jumpSpeed;
	}
	
	/**��������� ������.*/
	public void endJump(){
		jumping=false;
		super.resetInertionY();
	}
	
}
