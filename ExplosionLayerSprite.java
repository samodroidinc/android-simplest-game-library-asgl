package samodelkin.androidsimplestgamelibrary;

import android.content.res.Resources;

/**
 * ����� ��� �������� �������� ������.
 * @autor samodelkin
 * @version 1.0
 */
public class ExplosionLayerSprite extends LayerSprite {
	private boolean explosing;

	/** ������� ������ ������ � �������. */
	public ExplosionLayerSprite(int[] id, Resources res) {
		super(id, res);
		explosing = false;
		setVisible(false);
	}

	/** ������ �������� ������. */
	public void explose() {
		setVisible(true);
		explosing = true;
	}

	/**�������� ��������� ������.*/
	public boolean isExplosing(){
		return explosing;
	}
	
	/** ���������� � ������ ��������� ������. */
	public void update() {
		if (explosing){
			nextFrame();
			if (getFrameIndex()==0){
				explosing=false;
				this.setVisible(false);
			}
		}
	}
}
